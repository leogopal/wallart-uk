<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section.
 *
 * @package wallart
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<!-- Regular Injected Meta and Link items -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<meta name="google-site-verification" content="CkMRWWYYFvntbWgRUPzH83a8J6a40eaEvzeAFmYSJEI" />
<meta name="msvalidate.01" content="FF6C4C4F066E9CBD829ED6A9B7101E48" />
	<meta property="og:title" content="Wallart Studios" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="https://www.wallartstudios.co.uk/UKthumb.jpg" />
	<meta property="og:description" content="Your imagination is our creation" />

	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.png" />



	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<meta name="signet:authors" content="Leo Gopal, Richard Miles, Net Media Planets">
<meta name="signet:links" content="https://leogopal.com, https://github.com/leogopal, https://twitter.com/leogopal, https://richmiles.co.za/, https://github.com/richlloydmiles/, https://twitter.com/RichLloydMiles, http://nmp.co.za">

<!-- Start of wp_head() -->
<?php wp_head(); ?>
<!-- End of wp_head() -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-70247247-1', 'auto');
  ga('send', 'pageview');

</script>
</head>

<body <?php body_class(); ?>>
	<?php global $woocommerce;?>
	<div id="header" class="container">
		<div class="row">
			<div class="col-sm-4">
				<div class="header-contact">
					<div class="sa-tel">
						<img src="https://www.wallartstudios.com/wp-content/uploads/2016/01/great-britain-united-kingdom-wallart.jpg">
						<a href="tel:+447855322934">+44 7855 322 934</a>
					</div>
					<div class="uk-tel">
						<img src="https://www.wallartstudios.com/wp-content/uploads/2016/01/great-britain-united-kingdom-wallart.jpg">
						<a href="tel:+441491413194">+44 1491 413 194</a><br>
						<img src="https://www.wallartstudios.com/wp-content/uploads/2016/01/great-britain-united-kingdom-wallart.jpg">
						<a href="tel:+447855322934">+44 7855 322 934</a>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<a href="<?php bloginfo('url'); ?>" id="logo">
					<img src="<?php echo bloginfo('template_directory'); ?>/images/logo.png" />
				</a>	
			</div>
			<div class="col-sm-4" style="text-align:right;">

				<div class="header-cart">
				<i style="color:#5b435c;" class="fa fa-shopping-cart fa-2x"></i>
					<a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php echo sprintf (_n( '%d item', '%d items', WC()->cart->cart_contents_count ), WC()->cart->cart_contents_count ); ?> - <?php echo WC()->cart->get_cart_total(); ?></a>
				</div></br>
				<form role="search" method="get" class="woocommerce-product-search" action="<?php echo esc_url( home_url( '/'  ) ); ?>">
					<label class="screen-reader-text" for="s">Search for:</label>
					<input type="search" class="search-field" placeholder="Search Products&hellip;" value="" name="s" title="Search for:" />
					<input type="submit" value="Search" />
					<input type="hidden" name="post_type" value="product" />
				</form>
			</div>
		</div>

	</div>

	<nav class="navbar navbar-default">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<?php wp_nav_menu( array( 
					'theme_location' => 'topmenu',
					'items_wrap'      => '	<ul class="nav navbar-nav">%3$s</ul>',
					) ); ?>

				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
